Guia de Selectores
------------------------------------------
cuando se quieres crear un identificador
se usa la etiqueta id de la siguiente 
forma
#nombredelidentificador{
    poner los atributos
}
en los identificadores tratar de mantener
unicos para cada identificador
------------------------------------------
cuando se quiere crear una clase de poner
. antes del nombre

.nombredeclase{
    poner atributos
}
las clases pueden ser reutilizadas a 
diferencia de los "id"
------------------------------------------
selector * para todo el contenido
*{
    poner atributos
}
selector para todo el contenido donde se 
use
------------------------------------------
si se coloca entre llaves se puede usar 
como selector
primero indicar donde se esta usando y 
seleccionar el atributos

blockquode[cite]{
    atributos
}
